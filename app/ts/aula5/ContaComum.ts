import ManipuladorDeSaldo from './ManipuladorDeSaldo';

export default class ContaComum {
	private  manipulador:ManipuladorDeSaldo;

	public constructor() {
		this.manipulador = new ManipuladorDeSaldo();
	}

	 saca( valor:number) {
		this.manipulador.saca(valor);
	}

	deposita( valor:number) {
		this.manipulador.deposita(valor);
	}

	 rende() {
		this.manipulador.rende(1.1);
	}

	public  getSaldo() {
		return this.manipulador.getSaldo();
	}
}