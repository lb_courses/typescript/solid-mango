import ManipuladorDeSaldo from './ManipuladorDeSaldo';

// export default  class ContaDeEstudante extends ContaComum {
export default  class ContaDeEstudante {
	private  manipulador:ManipuladorDeSaldo;
	private  milhas: number;

	public constructor() {
		this.manipulador = new ManipuladorDeSaldo();
	}

	deposita( valor:number) {
		this.manipulador.deposita(valor);
		this.milhas += valor;
	}

	public  getMilhas() {
		return this.milhas;
	}
}
