import ServicoDeEntrega from './ServicoDeEntrega';

export default class Frete implements ServicoDeEntrega{
      para(cidade:string) {
        if("SAO PAULO" == cidade.toUpperCase()) {
            return 15;
        }
        return 30;
    }
}