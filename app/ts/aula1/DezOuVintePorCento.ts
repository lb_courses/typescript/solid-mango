import IRegraDeCalculo from './IRegraDeCalculo';
import Funcionario from './Funcionario';

export default class DezOuVintePorCento implements IRegraDeCalculo{

  public calcula (funcionario){
    if(funcionario.getSalarioBase() > 3000) {
      return funcionario.getSalarioBase() * 0.8
    } else {
      return funcionario.getSalarioBase() * 0.9;
    }
  }
  
}