import Cargo from './Cargo';

 export default class Funcionario {
	private  id:number;
	private  nome: string;
	private  cargo:Cargo;
	private dataDeAdmissao: Date;
	private salarioBase:number;
	
	public  getId() {
		return this.id;
	}
	public setId( id:number) {
		this.id = id;
	}
	public  getNome() {
		return  this.nome;
	}
  public  setNome( nome) {
		this.nome = nome;
	}
	public  getCargo() {
		return this.cargo;
	}
  public  setCargo( cargo:Cargo) {
		this.cargo = cargo;
	}
	public getDataDeAdmissao() {
		return this.dataDeAdmissao;
  }
  
  public  setDataDeAdmissao(dataDeAdmissao) {
		this.dataDeAdmissao = dataDeAdmissao;
	}
	public  getSalarioBase() {
		return this.salarioBase;
	}
  public  setSalarioBase( salarioBase) {
		this.salarioBase = salarioBase;
	}
	
	public  calcularSalario() {
		return this.cargo.getRegra().calcula(this);
	}
}
