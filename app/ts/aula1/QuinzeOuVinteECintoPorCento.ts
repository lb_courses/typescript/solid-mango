import IRegraDeCalculo from './IRegraDeCalculo';
import Funcionario from './Funcionario';

export default class DezQuinzeOuVinteECintoPorCentoouVintePorCento
implements IRegraDeCalculo {

  public calcula (funcionario:Funcionario){
    if( funcionario.getSalarioBase() > 2000) {
      return funcionario.getSalarioBase() * 0.75
    } else {
      return funcionario.getSalarioBase() * 0.85;
    }
  }  
}