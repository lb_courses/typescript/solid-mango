import { MeioDePagamento } from './MeioDePagamento'

export default class Pagamento {

    private  valor;
    private  forma:MeioDePagamento;

    public constructor( valor:number,  forma:MeioDePagamento) {
        this.valor = valor;
        this.forma = forma;
    }

    public  getValor() {
        return this.valor;
    }

     getForma() {
        return this.forma;
    }

    
    equals( obj:any) {
        if (!(obj instanceof Pagamento)) {
            return false;
        }
         const outro: Pagamento =  obj;
        // if (this.forma != outro.forma
        //         || .ToLongBits(valor) != 
        //                 .ToLongBits(outro.valor)) {
        if (this.forma != outro.forma) {
            return false;
        }

        return true;
    }

}
