export default class Fatura {

    private  cliente:string;
    private  valor:number;
    private  pagamentos:any;
    private  pago:any;

    public constructor( cliente,  valor) {
        this.cliente = cliente;
        this.valor = valor;
        this.pagamentos = [];
        this.pago = false;
    }

    public  getCliente() {
        return this.cliente;
    }

    public  getValor() {
        return this.valor;
    }

    getPagamentos() {
        return [].concat(this.pagamentos);
    }

     isPago() {
        return this.pago;
    }
    
    adicionaPagamento( pagamento){
    	this.pagamentos.add(pagamento);
    	if(this.valorTotalDosPagamentos() > this.valor){
    		this.pago = true;
    	}
    }

	private  valorTotalDosPagamentos() {
		 let total = 0;
		
		// for (Pagamento pagamento : pagamentos) {
		// 	total += pagamento.getValor();
    // }
    this.pagamentos.forEach(pagamento => {
      total += pagamento.getValor()
    });
		
		return total;
	}
}
