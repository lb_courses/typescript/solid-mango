import AcaoAposGerarNota from './AcaoAposGerarNota';
import Fatura from './Fatura';
import NotaFiscal from './NotaFiscal';

export default class GeradorDeNotaFiscal {

	private acoes: AcaoAposGerarNota[] = [];

    constructor (acoes: AcaoAposGerarNota[] ) {
		  this.acoes = acoes;        
    }

    public gera( fatura:Fatura) {

        const valor:number = fatura.getValorMensal();

        const  nf:NotaFiscal = new NotaFiscal(valor, this.impostoSimplesSobreO(valor));


        //Padrão Observer
        this.acoes.forEach( acaoAposGerarNota => {
          acaoAposGerarNota.executa(nf)
        } )  


        return nf;
    }

    impostoSimplesSobreO( valor:number):number {
        return valor * 0.06;
    }
}