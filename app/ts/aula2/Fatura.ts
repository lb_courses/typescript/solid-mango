
export default  class Fatura {

    private  valorMensal: number;
    private  cliente: string;

    //  Fatura() {}

     Fatura( valorMensal:number,  cliente:string) {
        this.valorMensal = valorMensal;
        this.cliente = cliente;
    }
      getValorMensal() {
        return this.valorMensal;
    }
     setValorMensal( valorMensal) {
        this.valorMensal = valorMensal;
    }
      getCliente() {
        return this.cliente;
    }
     setCliente( cliente) {
        this.cliente = cliente;
    }


}